<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 12/06/2016
 * Time: 2:35 PM
 */ ?>

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">User Orders</div>
                    <div class="panel-body">

                        @if (count($orders) > 0)
                            <table class="table table-striped task-table">

                                <!-- Table Headings -->
                                <thead>
                                <th>Order Id</th>
                                <th>Username</th>
                                <th>Name</th>
                                <th>Order Date</th>
                                <th>Status</th>
                                <th>Payed</th>
                                <th>Price</th>
                                <th>Comment</th>
                                <th>Command</th>

                                </thead>

                                <!-- Table Body -->
                                <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <!-- Task Name -->
                                        <td class="table-text">
                                            <div>{{ $order->id }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $order->user->username }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $order->user->name }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $order->created_at }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $order->status}}</div>
                                        </td>
                                        <td class="table-text">
                                            @if($order->payed)
                                                <div>Yes</div>
                                            @else
                                                <div>No</div>
                                            @endif
                                        </td>
                                        <td class="table-text">
                                            <div>${{ $order->totalPrice }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $order->comment }}</div>
                                        </td>
                                        <td>
                                            @if(!$order->payed)
                                                <a href="{{'order/payed/'. $order->id}}">Payed</a> |
                                            @endif
                                            @if(strcasecmp($order->status,'Shipped')!=0 )
                                                <a href="{{'order/shipped/'.$order->id}}">Shipped</a>
                                            @endif


                                        </td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$orders->links()}}

                        @else
                            <div align="center">No Orders</div>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection
