<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 12/06/2016
 * Time: 2:55 PM
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Checkout</div>

                    <div class="panel-body" align="center">
                        Thank you for shopping with us
                        <p>
                        Your Order Number {{$order->id}} has been placed , we will shipped your order as soon we get payment, Thanks.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

