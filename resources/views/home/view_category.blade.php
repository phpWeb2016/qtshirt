<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 7:32 PM
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-sm-3">
                    <div><h2>Categories</h2></div>
                    <div class="sidebar-nav">
                        <nav>
                            <ul class="nav">
                                @foreach ($categories as $category)
                                    <li><a href="{{url('view_category/'.$category->id)}}">{{$category->name}}</a></li>

                                @endforeach


                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-sm-9">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$category->name}}</div>


                    <div style="display: inline-block">
                        @foreach ($products as $product)
                            <div style="display: inline-block;padding:2em">

                                <div><a href="{{'view_product/'.$product->id}}"  ><img src="{{url($product->picture ) }}" style="width:250px;"></a></div>
                                <div style="font: bold 20px Georgia, serif">{{ $product->name }}</div>
                                <div style="font-style:oblique">${{$product->price}}</div>

                                @if ($product->unitInStock > 0)

                                    <div><a href="{{url('add_cart/'.$product->id)}}" id="{{$product->id}}" class="btn btn-primary">Add to Cart</a></div>

                                @else

                                    <div>Out of Stock</div>

                                @endif


                            </div>
                        @endforeach
                            {{$products->links()}}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

