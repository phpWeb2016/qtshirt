<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 2:28 PM
 * user cart Page
 */?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">My Cart</div>

                    <div class="panel-body">
                        @if (count($cart->cartItems) > 0)
                            <table class="table table-striped task-table">

                                <!-- Table Headings -->
                                <thead>
                                <th>Picture</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>&nbsp;</th>
                                </thead>

                                <!-- Table Body -->
                                <tbody>
                                @foreach ($cart->cartItems as $cartItem)
                                    <tr>
                                        <td class="table-text">
                                            <div><img src="{{url($cartItem->product->picture)}}"  style="width:200px;"></div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $cartItem->product->name }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>${{ $cartItem->product->price }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>{{ $cartItem->quantity }}</div>
                                        </td>
                                        <td class="table-text">
                                            <div>${{ $cartItem->product->price*$cartItem->quantity }}</div>
                                        </td>

                                        <td>
                                            <a href="{{url('remove_cart/'.$cartItem->product->id)}}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total Without GST</td>
                                    <td><b>${{$total}}</b></td>
                                    <td></td>


                                </tr>
                                <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total With GST</td>
                                    <td><b>${{$total*1.15}}</b></td>
                                <td><div><a href="{{url('clear_cart')}}"  class="btn btn-danger">Clear</a>
                                        <a href="{{url('checkout')}}"  class="btn btn-primary">CheckOut</a></div></td>


                                </tr>
                                </tbody>
                            </table>
                            @else
                            <div align="center">No Products in My Cart</div>

                        @endif

                    </div>

                </div>

            </div>
        </div>
    </div>
@endsection
