<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 2:26 PM
 * Contact Us page
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Contact Us</div>

                    <div class="panel-body">
                        <address>
                            Quality T-Shirt<br />
                            Unitec,Auckland,New Zealand<br />
                            <abbr title="Phone">P:</abbr>
                            02102220975
                        </address>

                        <address>
                            <strong>Support:</strong>   <a href="mailto:Support@unitec.com">Support@unitec.com</a><br />
                            <strong>Marketing:</strong> <a href="mailto:Marketing@unitec.com">Marketing@unitec.com</a>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

