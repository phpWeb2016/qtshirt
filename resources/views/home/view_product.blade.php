<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 6:56 PM
 */?>


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>{{ $product->name }}</h2></div>

                    <div class="panel-body" align="center">
                        <div><img src="{{url($product->picture) }}" ></div>

                        <dl class="dl-horizontal">
                            <dt>
                                Description
                            </dt>

                            <dd>
                                <div>{{ $product->description }}</div>
                            </dd>

                            <dt>
                                Price
                            </dt>

                            <dd>
                                <div>${{ $product->price }}</div>
                            </dd>


                        </dl>
                        @if ($product->unitInStock > 0)

                            <div><a href="{{url('add_cart/'.$product->id)}}" id="{{$product->id}}" class="btn btn-primary">Add to Cart</a></div>

                        @else

                            <div>Out of Stock</div>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
