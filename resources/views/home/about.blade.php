<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 2:26 PM
 * About us page
 */
?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">About</div>

                    <div class="panel-body">
                        <p>
                           <h2>Quality T-shirt </h2><br/>
                           <h4>Done by Thaer Serhan</h4>
                        Email: thaer89@gmail.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

