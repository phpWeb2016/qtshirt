<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 2:27 PM
 * user profile page
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">My Profile</div>

                    <div class="panel-body">
                        <dl class="dl-horizontal">
                            <dt>
                           Name
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->name }}</div>
                            </dd>

                            <dt>
                               Username
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->username }}</div>
                            </dd>

                            <dt>
                                Email
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->email }}</div>
                            </dd>

                            <dt>
                               Mobile Phone
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->mobilePhone }}</div>
                            </dd>

                            <dt>
                                Work phone
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->workPhone }}</div>
                            </dd>

                            <dt>
                                Home Phone
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->homePhone }}</div>
                            </dd>

                            <dt>
                                address
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->address }}</div>
                            </dd>

                            <dt>
                                Postal Code
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->postalCode }}</div>
                            </dd>


                            <dt>
                                City
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->city }}</div>
                            </dd>
                            <dt>
                                Country
                            </dt>

                            <dd>
                                <div>{{ Auth::User()->country }}</div>
                            </dd>




                        </dl>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
