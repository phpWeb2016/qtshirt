<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 2/06/2016
 * Time: 9:40 PM
 */
?>
@if (count($errors) > 0)
    <!-- Form Error List -->
    <div class="alert alert-danger">
        <strong>Whoops! Something went wrong!</strong>

        <br><br>

        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif