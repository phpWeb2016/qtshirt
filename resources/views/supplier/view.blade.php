<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 3:07 PM
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">View Supplier {{ $supplier->name }}</div>

                    <div class="panel-body">

                        <dl class="dl-horizontal">
                            <dt>
                                Name
                            </dt>

                            <dd>
                                <div>{{ $supplier->name }}</div>
                            </dd>

                            <dt>
                                Email
                            </dt>

                            <dd>
                                <div>{{ $supplier->email }}</div>
                            </dd>

                            <dt>
                                Mobile Phone
                            </dt>

                            <dd>
                                <div>{{ $supplier->mobilePhone }}</div>
                            </dd>

                            <dt>
                                Work phone
                            </dt>

                            <dd>
                                <div>{{ $supplier->workPhone }}</div>
                            </dd>

                            <dt>
                                Home Phone
                            </dt>

                            <dd>
                                <div>{{ $supplier->homePhone }}</div>
                            </dd>

                            <dt>
                                address
                            </dt>

                            <dd>
                                <div>{{ $supplier->address }}</div>
                            </dd>

                            <dt>
                                Postal Code
                            </dt>

                            <dd>
                                <div>{{ $supplier->postalCode }}</div>
                            </dd>


                            <dt>
                                City
                            </dt>

                            <dd>
                                <div>{{ $supplier->city }}</div>
                            </dd>
                            <dt>
                                Country
                            </dt>

                            <dd>
                                <div>{{ $supplier->country }}</div>
                            </dd>

                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
