<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 6/06/2016
 * Time: 4:03 PM
 */
?>

@extends('layouts.app')

@section('content')



    <div class="panel-body">
        <h2>Supplier</h2>
        <p>
            <a href="{{url('supplier/create')}}">Create supplier</a>
        </p>
        <table class="table">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    Address
                </th>

                <th></th>
            </tr>


            @foreach ($suppliers as $supplier)
                <tr>
                    <td>
                        <div>{{ $supplier->name }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->email }}</div>
                    </td>
                    <td>
                        <div>{{ $supplier->address }}</div>
                    </td>
                    <td>
                        <a href="{{url('supplier/'.$supplier->id)}}">View</a> |
                        <a href="{{url('supplier/delete/'.$supplier->id)}}">Delete</a>
                    </td>


                </tr>
            @endforeach

        </table>
    </div>
@endsection