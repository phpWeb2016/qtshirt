<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 6/06/2016
 * Time: 4:03 PM
 */
?>

@extends('layouts.app')

@section('content')



    <div class="panel-body">
        <h2>Categories</h2>
        <p>
            <a href="{{url('category/create')}}">Create category</a>
        </p>
        <table class="table">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Description
                </th>

                <th></th>
            </tr>


            @foreach ($categories as $category)
                <tr>
                    <td>
                        <div>{{ $category->name }}</div>
                    </td>
                    <td>
                        <div>{{ $category->description }}</div>
                    </td>
                    <td>
                        <a href="{{url('category/delete/'.$category->id)}}">Delete</a>
                    </td>

                </tr>
            @endforeach

        </table>
    </div>
@endsection