<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 12:46 PM
 */
 ?>

@extends('layouts.app')

@section('content')
 <div class="container">
  <div class="row">
   <div class="col-md-10 col-md-offset-1">
    <div class="panel panel-default">
     <div class="panel-heading">Welcome</div>

     <div class="panel-body">
      <p>
       <a href="{{url('product')}}">Manage products</a>
      </p>
      <p>
       <a href="{{url('category')}}">Manage categories</a>
      </p>
      <p>
       <a href="{{url('supplier')}}">Manage suppliers</a>
      </p>
      <p>
       <a href="{{url('user')}}">Manage users</a>
      </p>
      <p>
       <a href="{{url('order')}}">Manage orders</a>
      </p>
     </div>
    </div>
   </div>
  </div>
 </div>
@endsection
