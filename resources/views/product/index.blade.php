<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 2/06/2016
 * Time: 9:51 PM
 */
?>

@extends('layouts.app')

@section('content')


        <div class="panel-body">
            <div>
                <a href="{{'product/create'}}">Create product</a>
            </div>
            @if (count($products) > 0)
            <table class="table table-striped task-table">

                <!-- Table Headings -->
                <thead>
                <th>Product Name</th>
                <th>Category</th>
                <th>Supplier</th>
                <th>Price</th>
                <th>Picture</th>
                <th>&nbsp;</th>
                </thead>

                <!-- Table Body -->
                <tbody>
                @foreach ($products as $product)
                    <tr>
                        <!-- Task Name -->
                        <td class="table-text">
                            <div>{{ $product->name }}</div>
                        </td>
                        <td class="table-text">
                            <div>{{ $product->category['name'] }}</div>
                        </td>
                        <td class="table-text">
                            <div>{{ $product->supplier['name'] }}</div>
                        </td>
                        <td class="table-text">
                            <div>${{ $product->price }}</div>
                        </td>


                        <td class="table-text">
                            <div><img src="{{url($product->picture ) }}"  style="height:228px;"></div>
                        </td>

                        <td>
                            <a href="{{'product/delete/'.$product->id}}">Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
                {{$products->links()}}
            @endif
        </div>


@endsection