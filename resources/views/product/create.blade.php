@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
    @include('common.errors')

    <!-- New Product Form -->
        <form action="{{ url('product') }}" method="post" class="form-horizontal" enctype="multipart/form-data" >
        {{ csrf_field() }}

        <!-- Product Name -->
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>

                <div class="col-sm-6">
                    <input type="text" name="name" id="product-name" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="Price" class="col-sm-3 control-label">Price</label>

                <div class="col-sm-6">
                    <input type="text" name="price" id="product-price" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="unitInStock" class="col-sm-3 control-label">Unit In Stock</label>

                <div class="col-sm-6">
                    <input type="text" name="unitInStock" id="product-unitInStock" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="Category" class="col-sm-3 control-label">Category</label>

                <div class="col-sm-6">
                    {!! Form::select('category', $categories) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="Category" class="col-sm-3 control-label">Supplier</label>

                <div class="col-sm-6">
                    {!! Form::select('supplier', $suppliers) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="Image" class="col-sm-3 control-label">Upload Image</label>

                <div class="col-sm-6">
                    <input type="file" name="file" id="file">
                </div>
            </div>
            <input type="hidden" value="{{ csrf_token() }}" name="_token">


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> Add Product
                    </button>

                </div>
            </div>
        </form>
    </div>


@endsection