<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 4:18 PM
 */ ?>

<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 6/06/2016
 * Time: 4:03 PM
 */
?>

@extends('layouts.app')

@section('content')



    <div class="panel-body">
        <h2>Supplier</h2>
        <table class="table">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    Address
                </th>
                <th>
                    Type
                </th>
                <th>
                    Status
                </th>


                <th></th>
            </tr>


            @foreach ($users as $user)
                <tr>
                    <td>
                        <div>{{ $user->name }}</div>
                    </td>
                    <td>
                        <div>{{ $user->email }}</div>
                    </td>
                    <td>
                        <div>{{ $user->address }}</div>
                    </td>
                    <td>
                        <div>{{ $user->type }}</div>
                    </td>
                    <td>
                        @if($user->active)
                            Enabled
                        @else
                            Disabled
                        @endif
                    </td>
                    <td>
                        <a href="{{url('user/'.$user->id)}}">View</a> |
                        @if($user->active)
                            <a href="{{'user/disable/'.$user->id}}">Disable</a>
                        @else
                            <a href="{{url('/user/enable/'.$user->id) }}">Enable</a>
                        @endif
                    </td>


                </tr>
            @endforeach

        </table>
    </div>
@endsection
