<?php
/**
 * Created by PhpStorm.
 * User: Thaer
 * Date: 11/06/2016
 * Time: 3:08 PM
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">View user {{ $user->name }}</div>

                    <div class="panel-body">
                        <dl class="dl-horizontal">
                            <dt>
                                Name
                            </dt>

                            <dd>
                                <div>{{ $user->name }}</div>
                            </dd>

                            <dt>
                                Username
                            </dt>

                            <dd>
                                <div>{{ $user->username }}</div>
                            </dd>

                            <dt>
                                Email
                            </dt>

                            <dd>
                                <div>{{ $user->email }}</div>
                            </dd>

                            <dt>
                                Mobile Phone
                            </dt>

                            <dd>
                                <div>{{$user->mobilePhone }}</div>
                            </dd>

                            <dt>
                                Work phone
                            </dt>

                            <dd>
                                <div>{{ $user->workPhone }}</div>
                            </dd>

                            <dt>
                                Home Phone
                            </dt>

                            <dd>
                                <div>{{ $user->homePhone }}</div>
                            </dd>

                            <dt>
                                address
                            </dt>

                            <dd>
                                <div>{{ $user->address }}</div>
                            </dd>

                            <dt>
                                Postal Code
                            </dt>

                            <dd>
                                <div>{{ $user->postalCode }}</div>
                            </dd>


                            <dt>
                                City
                            </dt>

                            <dd>
                                <div>{{ $user->city }}</div>
                            </dd>
                            <dt>
                                Country
                            </dt>

                            <dd>
                                <div>{{ $user->country }}</div>
                            </dd>

                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
