<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('categories')->insert([
            'name' => 'Men',
            'description' => 'Men',

        ]);
        DB::table('categories')->insert([
            'name' => 'Women',
            'description' => 'Women',

        ]);
    }
}
