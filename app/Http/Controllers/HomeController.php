<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application Home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'asc')->take(10)->get();
        $categories = Category::all();
        return view('home',[
            'products' => $products,'categories' => $categories
        ]);
    }
    public function admin()
    {
        $this->authorize('admin');
        return view('admin_page');
    }
    public function about()
    {

        return view('home/about');
    }
    public function contactUs()
    {

        return view('home/contact_us');
    }
    public function userProfile()
    {

        return view('home/my_profile');
    }
    public function viewProduct($id)
    {
        $product = Product::find($id);
        return view('home/view_product',['product' => $product]);
    }
    public function viewCategories()
    {
        $categories = Category::all();
        return view('home/view_categories',['categories' => $categories]);
    }
    public function viewCategoryProducts($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        $products = Product::where('category_id', $id)->paginate(10);
        return view('home/view_category',[
            'products' => $products,'category'=>$category
        ,'categories' => $categories]);
    }
    public function viewMyOrder()
    {
        $user=Auth::User();
        $orders = Order::where('user_id',$user->id)->paginate(5);
        return view('home/view_my_orders',['orders' => $orders]);
    }
}
