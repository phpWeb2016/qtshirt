<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoryController extends Controller
{
    //
    public function index()
    {
        return view('category/index',[
            'categories' => Category::all()
        ]);
    }

    public function create()
    {
        $this->authorize('admin');
        return view('category/create');
    }

    public function store(Request $request)
    {
        $this->authorize('admin');
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);


        $category = new Category();
        $category->name = $request->name;
        $category->description=$request->description;
        $category->save();

        return redirect('category');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function delete($id)
    {
        $this->authorize('admin');
        $category = Category::find($id);
        $category->delete();
        return redirect('category');
    }




}
