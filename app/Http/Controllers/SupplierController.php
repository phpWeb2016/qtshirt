<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

use App\Http\Requests;

class SupplierController extends Controller
{
    //
    public function index()
    {
        $this->authorize('admin');
        return view('supplier/index',[
            'suppliers' => Supplier::all()
        ]);
    }

    public function create()
    {
        $this->authorize('admin');
        return view('supplier/create');
    }

    public function store(Request $request)
    {
        $this->authorize('admin');
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'address' => 'required|max:255',
            'postalCode' => 'required|max:25',
            'city' => 'required|max:55',
            'country' => 'required|max:50',
        ]);


        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->email = $request->email;
        $supplier->address = $request->address;
        $supplier->postalCode = $request->postalCode;
        $supplier->city = $request->city;
        $supplier->country = $request->country;
        $supplier->mobilePhone = $request->mobilePhone;
        $supplier->workPhone = $request->workPhone;
        $supplier->homePhone = $request->homePhone;
        $supplier->save();

        return redirect('supplier');
    }

    public function show($id)
    {
        $this->authorize('admin');
        $supplier = Supplier::findOrFail($id);
        return view('supplier/view', [
            'supplier' => $supplier
        ]);

    }
    public function delete($id)
    {
        $this->authorize('admin');
        $supplier = Supplier::find($id);
        $supplier->delete();
        return redirect('supplier');
    }


    public function __construct()
    {
        $this->middleware('auth');
    }



}
