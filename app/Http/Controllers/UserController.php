<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $this->authorize('admin');
        $users = User::orderBy('created_at', 'asc')->get();
        return view('user/index', [
            'users' => $users
        ]);
    }

    public function show($id)
    {
        $this->authorize('admin');
        $user = User::findOrFail($id);
        return view('user/view', [
            'user' => $user
        ]);

    }
    public function disable($id)
    {
        $this->authorize('admin');
        $user = User::findOrFail($id);
        $user->active=false;
        $user->save();

        return redirect('user');

    }
    public function enable($id)
    {
        $this->authorize('admin');
        $user = User::findOrFail($id);
        $user->active=true;
        $user->save();
        return redirect('user');

    }
}
