<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $this->authorize('admin');
        return view('order.index',[
            'orders' => Order::paginate(10)
        ]);
    }
    public function payed($id)
    {

        $order=Order::find($id);
        $order->payed=true;
        $order->save();
        return redirect('order');

    }
    public function shipped($id)
    {
        $order=Order::find($id);

        $order->status='Shipped';
        $order->save();
        Log::info('Order shipped: '.$order->id);
        try{
            Mail::send('mail.order_shipped', ['order' => $order], function ($message) use($order) {
                $message->from('serhat01@myunitec.com', 'Quality T-shirt')->subject("Order Shipped");
                $message->to($order->user->email);
            });
        }catch (Exception $e) {
            Log::error('Caught exception: '.$e->getMessage());

        }

        return redirect('order');

    }
    
    
}
