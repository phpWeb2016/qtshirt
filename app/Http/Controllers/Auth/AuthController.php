<?php

namespace App\Http\Controllers\Auth;


use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'username' => 'required|max:255|unique:users',
            'address' => 'required|max:255',
            'mobilePhone' => 'required_without_all:workPhone,homePhone|digits_between:5,15',
            'workPhone' => 'required_without_all:mobilePhone,homePhone|digits_between:5,15',
            'homePhone' => 'required_without_all:workPhone,mobilePhone|digits_between:5,15',
            'postalCode' => 'required|max:25',
            'city' => 'required|max:55',
            'country' => 'required|max:50',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $user= User::create([
            'name' => $data['name'],
            'username'=>$data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'address'=>$data['address'],
            'postalCode'=>$data['postalCode'],
            'city'=>$data['city'],
            'country'=>$data['country'],
            'mobilePhone'=>$data['mobilePhone'],
            'workPhone'=>$data['workPhone'],
            'homePhone'=>$data['homePhone'],
            'type'=>'user',
            'active'=>true,

        ]);

        if($user)
        {
            Log::info('User Created: '.$data['username']);
            try {
                Mail::send('mail.user_created', ['user' => $user], function ($message) use ($user) {
                    $message->from('serhat01@myunitec.com', 'Quality T-shirt')->subject("Welcome to Quality Tshirt");
                    $message->to($user->email);
                });

            }catch (Exception $e) {
                Log::error('Caught exception: '.$e->getMessage());

            }
        }

        return $user;

    }
    public function authenticate(Request $request)
    {
        $user = $request->user;
        $password=$request->password;
        $validator = Validator::make($request->all(), [
            'user' => 'required|max:255',
            'password' =>'required'
        ]);

        if (Auth::attempt(['username' => $user, 'password' => $password, 'active' => 1])) {
            // Authentication passed...
            Log::info('User login: '.$user);
            return redirect()->intended('/home');
        }else{
            $validator->errors()->add('user', 'Username and  password is not match or user is disabled!');
            return redirect('login')
                ->withErrors($validator);

        }

    }


}
